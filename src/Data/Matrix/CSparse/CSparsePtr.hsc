{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE CPP, ForeignFunctionInterface #-}
{-# LANGUAGE Rank2Types, ScopedTypeVariables #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Data.Matrix.CSparse.CSparsePtr where

------------------------------------------------------------------------------
--
--  Bindings to the `CSparse` concise sparse-matrix library.
--
--  Changelog:
--   + 04/09/2015  --  initial file;
--

-- import GHC.Types (SPEC(..))
-- import GHC.Int
import Foreign as F
import Foreign.C.Types
import System.IO.Unsafe
import Control.Monad
import Data.Bool
import Data.Vector.Storable (Vector)
import qualified Data.Vector.Storable as Vec
-- import qualified Data.Vector.Generic as G
import Data.Vector.Helpers as H
import Data.Matrix.Util

#include <suitesparse/cs.h>


-- * Concise Sparse-matrix library data-structures.
------------------------------------------------------------------------------
newtype CSparse = CSparse { ptrCSparse :: Ptr CSparse }
                deriving (Eq, Show, Storable)

type CSLong = #{type cs_long_t}


-- * Foreign bindings.
------------------------------------------------------------------------------
foreign import ccall unsafe "suitesparse/cs.h cs_spalloc"
  cs_spalloc :: CSLong -> CSLong -> CSLong -> CSLong -> CSLong -> IO CSparse

foreign import ccall unsafe "suitesparse/cs.h cs_spfree"
  cs_spfree :: CSparse -> IO ()

foreign import ccall unsafe "suitesparse/cs.h cs_free"
  cs_free :: Ptr a -> IO ()

foreign import ccall unsafe "suitesparse/cs.h &cs_free"
  p_cs_free :: FunPtr (Ptr a -> IO ())

foreign import ccall unsafe "suitesparse/cs.h &cs_spfree"
  p_cs_spfree :: FunPtr (CSparse -> IO ())

foreign import ccall unsafe "suitesparse/cs.h cs_add"
  cs_add :: CSparse -> CSparse -> Double -> Double -> CSparse

foreign import ccall unsafe "suitesparse/cs.h cs_multiply"
  cs_multiply :: CSparse -> CSparse -> CSparse

foreign import ccall unsafe "suitesparse/cs.h cs_transpose"
  cs_transpose :: CSparse -> CInt -> CSparse

-- foreign import ccall unsafe "stdio.h malloc" c_malloc :: CSize -> IO (Ptr a)
-- foreign import ccall unsafe "stdio.h free"   c_free   :: Ptr a -> IO ()


-- * Bound CSparse API functions.
------------------------------------------------------------------------------
cs'spalloc :: Int -> Int -> Int -> IO CSparse
{-# INLINE cs'spalloc #-}
cs'spalloc m n nzmax = cs_spalloc (fi m) (fi n) (fi nzmax) 1 0

{-- }
cs'spalloc :: Int -> Int -> Int -> IO CSparse
cs'spalloc m n nzmax = do
  p <- c_malloc #{size cs_dl}
  c <- c_malloc $ sizeOf (undefined :: Int)*(n+1)
  i <- c_malloc $ sizeOf (undefined :: Int)*nzmax
  x <- c_malloc $ sizeOf (undefined :: Double)*nzmax
  #{poke cs_dl, nzmax} p nzmax
  #{poke cs_dl, m} p m
  #{poke cs_dl, n} p n
  #{poke cs_dl, p} p c
  #{poke cs_dl, i} p i
  #{poke cs_dl, x} p x
  #{poke cs_dl, nz} p (-1)
  return $ CSparse p
--}

cs'transpose :: CSparse -> CSparse
{-# INLINE cs'transpose #-}
cs'transpose  = flip cs_transpose 1

-- | First scale the two given matrices, then add them.
cs'add :: CSparse -> CSparse -> CSparse
{-# INLINE cs'add #-}
cs'add x y = cs_add x y 1 1

cs'multiply :: CSparse -> CSparse -> CSparse
{-# INLINE cs'multiply #-}
cs'multiply  = cs_multiply


-- * Getters and setters.
------------------------------------------------------------------------------
cs'setCols :: Vector Int -> CSparse -> IO Bool
{-# INLINE cs'setCols #-}
cs'setCols vs (CSparse p) = do
  cc <- (+1) <$> #{peek cs_dl, n} p
  bool (return False)
    (#{peek cs_dl, p} p >>= \q -> fromv q cc vs >> return True) (cc == len vs)

cs'setRows :: Vector Int -> CSparse -> IO Bool
{-# INLINE cs'setRows #-}
cs'setRows vs (CSparse p) = do
  nz <- #{peek cs_dl, nzmax} p
  bool (return False)
    (#{peek cs_dl, i} p >>= \q -> fromv q nz vs >> return True) (nz == len vs)

cs'setVals :: Vector Double -> CSparse -> IO Bool
{-# INLINE cs'setVals #-}
cs'setVals vs (CSparse p) = do
  nz <- #{peek cs_dl, nzmax} p
  bool (return False)
    (#{peek cs_dl, x} p >>= \q -> fromv q nz vs >> return True) (nz == len vs)
