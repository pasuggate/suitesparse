{-# LANGUAGE PatternSynonyms, ViewPatterns #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Matrix.CSparse
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Just renames `CXSparse` to `CSparse`, because `CSparse` is now deprecated?
-- 
-- Changelog:
--  + 04/09/2015  --  initial file;
--  + 10/06/2017  --  changed to just a wrapper over `CXSparse`;
-- 
-- TODO:
--  + do the class instances (and constraints) work correctly, or should I
--    use a `newtype`?
-- 
------------------------------------------------------------------------------

module Data.Matrix.CSparse
       ( CSparse, pattern CSparse
       , module Data.Matrix.CXSparse
       ) where

import Data.Vector.Helpers (IVec, DVec)
import Data.Matrix.CXSparse


------------------------------------------------------------------------------
-- | Now just a type synonym.
type CSparse = CXSparse

pattern CSparse :: Int -> Int -> Int -> IVec -> IVec -> DVec -> Maybe Int ->
                   CXSparse
pattern CSparse nz rn cn p i xs nn = CXSparse nz rn cn p i xs nn
