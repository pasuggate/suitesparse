{-# LANGUAGE BangPatterns, RecordWildCards #-}
{-# LANGUAGE CPP, ForeignFunctionInterface #-}
{-# LANGUAGE Rank2Types, ScopedTypeVariables #-}
{-# LANGUAGE GeneralizedNewtypeDeriving, FlexibleInstances #-}
module Data.Matrix.CholMod.Bindings
       ( module Data.Matrix.CholMod.Bindings
       ) where

------------------------------------------------------------------------------
--
--  Operations useful for writing monadic, sparse-matrix code.
--
--  Changelog:
--   + 14/09/2015  --  initial file (refactored);
--

import Foreign as F
import Foreign.C.Types hiding (CBool)
import Foreign.C.String
import Control.Monad ((>=>))
import Control.Monad.Trans.Class
import Control.Monad.IO.Class
import Data.Bool
import Data.Matrix.Util

#include <suitesparse/cholmod.h>


-- * CholMod data-structures.
------------------------------------------------------------------------------
-- | CholMod workspace data-structure.
--   
--   TODO: Should this just be "opaque;" i.e., do I ever need to set/inpect
--     its values?
newtype CMCommon =
  CMCommon { ptrCMCommon :: Ptr CMCommon }
  deriving (Eq, Show) -- , Storable)


-- ** Internal data-types, for interfacing with CHOLMOD.
------------------------------------------------------------------------------
-- | Sparse matrices that are managed via CholMod.
newtype PtrSparse =
  PtrSparse { ptrSparse :: Ptr PtrSparse }
  deriving (Eq, Show, Storable)

------------------------------------------------------------------------------
-- | Factor matrices that are managed via CholMod.
newtype PtrFactor =
  PtrFactor { ptrFactor :: Ptr PtrFactor }
  deriving (Eq, Show, Storable)

------------------------------------------------------------------------------
-- | Dense matrices that are managed via CholMod.
newtype PtrDense =
  PtrDense  { ptrDense  :: Ptr PtrDense  }
  deriving (Eq, Show, Storable)


-- ** Wrapped versions of CHOLMOD's data-types.
------------------------------------------------------------------------------
type SSLong = #{type SuiteSparse_long}

------------------------------------------------------------------------------
-- | Sparse-matrix mode data-types.
newtype SType = SType CInt deriving (Eq, Show, Storable)
newtype IType = IType CInt deriving (Eq, Show, Storable)
newtype XType = XType CInt deriving (Eq, Show, Storable)
newtype DType = DType CInt deriving (Eq, Show, Storable)

#{enum SType, SType
 , cholmod_s_lower = (-1)
 , cholmod_s_unsym = 0
 , cholmod_s_upper = 1
 }

#{enum IType, IType
 , cholmod_int     = CHOLMOD_INT
 , cholmod_intlong = CHOLMOD_INTLONG
 , cholmod_long    = CHOLMOD_LONG
 }

#{enum XType, XType
 , cholmod_pattern = CHOLMOD_PATTERN
 , cholmod_real    = CHOLMOD_REAL
 , cholmod_complex = CHOLMOD_COMPLEX
 , cholmod_zomplex = CHOLMOD_ZOMPLEX
 }

#{enum DType, DType
 , cholmod_double  = CHOLMOD_DOUBLE
 , cholmod_single  = CHOLMOD_SINGLE
 }

------------------------------------------------------------------------------
-- | Matrix pattern-types.
newtype Pattern = Pattern CInt deriving (Eq, Show, Storable)

#{enum Pattern, Pattern
 , pattern_numerical = 1    /* -- numerical values are computed */
 , pattern_symbolic  = 0    /* -- only the sparsity-pattern is computed */
 , pattern_nodiag    = -1   /* -- symbolic and strictly-lower elements */
 , pattern_padded    = -2   /* -- no diagonal and +50% free space */
 }

------------------------------------------------------------------------------
-- | Data-type for choosing what sort of values are produced, via a sparse-
--   matrix function.
newtype Values = Values CInt deriving (Eq, Show, Storable)

#{enum Values, Values
 , values_pattern    = 0    /* -- symbolic operation only */
 , values_array      = 1    /* -- treat the values simply as array elements */
 , values_conjugate  = 2    /* -- conjugate the values */
 }

------------------------------------------------------------------------------
-- | Solver mode, for solving a system of linear equations.
newtype SolveMode = SolveMode CInt deriving (Eq, Show, Storable)

#{enum SolveMode, SolveMode
 , solve_A           = 0    /* -- solve Ax=b */
 , solve_LDLt        = 1    /* -- solve LDL'x=b */
 , solve_LD          = 2    /* -- solve LDx=b */
 , solve_DLt         = 3    /* -- solve DL'x=b */
 , solve_L           = 4    /* -- solve Lx=b */
 , solve_Lt          = 5    /* -- solve L'x=b */
 , solve_D           = 6    /* -- solve Dx=b */
 , solve_P           = 7    /* -- permute x=Pb */
 , solve_Pt          = 8    /* -- permute x=P'b */
 }


-- * C Booleans.
------------------------------------------------------------------------------
-- | Booleans compatible with CholMod.
--   NOTE: Only supports `CInt's.
newtype CBool = CBool CInt deriving (Eq, Show, Storable)
#{enum CBool, CBool, ctrue = 1, cfalse = 0}

fromCBool :: CBool -> Bool
{-# INLINE fromCBool #-}
fromCBool  = (ctrue ==)

toCBool :: Bool -> CBool
{-# INLINE toCBool #-}
toCBool  = bool cfalse ctrue


-- * Monad (transformer) for sequencing CholMod computations.
------------------------------------------------------------------------------
-- | The `CholMod` monad allows the CholMod workspace to be threaded through
--   computations.
--   NOTE: Equivalent to the `ReaderT` monad transformer?
newtype CholModT m a = CholModT { runCholModT :: CMCommon -> m a }
type    CM       m a = CholModT m a

------------------------------------------------------------------------------
-- | Some standard instances.
instance Monad m => Functor (CholModT m) where
  {-# INLINE fmap #-}
  fmap f (CholModT k) = CholModT $ fmap f . k

instance Monad m => Applicative (CholModT m) where
  {-# INLINE pure #-}
  pure  x = CholModT $ return . const x
  {-# INLINE (<*>) #-}
  f <*> k = CholModT $ \cm -> runCholModT f cm <*> runCholModT k cm

instance Monad m => Monad (CholModT m) where
  {-# INLINE return #-}
  return  = pure
  {-# INLINE (>>=) #-}
  m >>= k = CholModT $ \cm -> runCholModT m cm >>= flip runCholModT cm . k

instance MonadIO m => MonadIO (CholModT m) where
  {-# INLINE liftIO #-}
  liftIO m = CholModT $ const (liftIO m)

instance MonadTrans CholModT where
  {-# INLINE lift #-}
  lift m = CholModT $ const m


-- * Foreign bindings.
------------------------------------------------------------------------------
-- | Allocates a CHOLMOD workspace.
foreign import ccall unsafe "suitesparse/cholmod.h cholmod_start"
  cholmod_start  :: CMCommon -> IO CBool

foreign import ccall unsafe "suitesparse/cholmod.h cholmod_finish"
  cholmod_finish :: CMCommon -> IO CBool


-- ** Sparse matrix routines.
------------------------------------------------------------------------------
-- | Allocate an empty sparse-matrix.
foreign import ccall unsafe "suitesparse/cholmod.h cholmod_allocate_sparse"
  cholmod_allocate_sparse ::
    CSize -> CSize -> CSize -> CBool -> CBool -> SType -> XType -> CMCommon ->
    IO PtrSparse

foreign import ccall unsafe "suitesparse/cholmod.h cholmod_free_sparse"
  cholmod_free_sparse :: Ptr PtrSparse -> CMCommon -> IO ()

foreign import ccall unsafe "suitesparse/cholmod.h cholmod_copy_sparse"
  cholmod_copy_sparse :: PtrSparse -> CMCommon -> IO PtrSparse

foreign import ccall unsafe "suitesparse/cholmod.h cholmod_copy"
  cholmod_copy :: PtrSparse -> SType -> Pattern -> CMCommon -> IO PtrSparse

foreign import ccall unsafe "suitesparse/cholmod.h cholmod_read_sparse"
  cholmod_read_sparse :: FILE -> CMCommon -> IO PtrSparse

foreign import ccall unsafe "suitesparse/cholmod.h cholmod_write_sparse"
  cholmod_write_sparse ::
    FILE -> PtrSparse -> PtrSparse -> Ptr Char -> CMCommon -> IO ()

foreign import ccall unsafe "suitesparse/cholmod.h cholmod_print_sparse"
  cholmod_print_sparse :: PtrSparse -> CString -> CMCommon -> IO CBool

foreign import ccall unsafe "suitesparse/cholmod.h cholmod_speye"
  cholmod_speye :: CSize -> CSize -> XType -> CMCommon -> IO PtrSparse

foreign import ccall unsafe "suitesparse/cholmod.h cholmod_nnz"
  cholmod_nnz :: PtrSparse -> CMCommon -> IO SSLong

foreign import ccall unsafe "suitesparse/cholmod.h cholmod_sort"
  cholmod_sort :: PtrSparse -> CMCommon -> IO CBool

-- *** Matrix (re)shaping routines.
------------------------------------------------------------------------------
foreign import ccall unsafe "suitesparse/cholmod.h cholmod_transpose"
  cholmod_transpose :: PtrSparse -> Values -> CMCommon -> IO PtrSparse

foreign import ccall unsafe "suitesparse/cholmod.h cholmod_ptranspose"
  cholmod_ptranspose ::
    PtrSparse -> Values -> Ptr CInt -> Ptr CInt -> CSize -> CMCommon ->
    IO PtrSparse

foreign import ccall unsafe "suitesparse/cholmod.h cholmod_submatrix"
  cholmod_submatrix ::
    PtrSparse -> Ptr CInt -> SSLong -> Ptr CInt -> SSLong -> CBool -> CBool ->
    CMCommon -> IO PtrSparse

-- *** Matrix algebraic routines.
------------------------------------------------------------------------------
foreign import ccall unsafe "suitesparse/cholmod.h cholmod_aat"
  cholmod_aat ::
    PtrSparse -> Ptr CInt -> CSize -> Pattern -> CMCommon -> IO PtrSparse

foreign import ccall unsafe "suitesparse/cholmod.h cholmod_add"
  cholmod_add ::
    PtrSparse -> PtrSparse -> Ptr Double -> Ptr Double ->
    CBool -> CBool -> CMCommon -> IO PtrSparse

foreign import ccall unsafe "suitesparse/cholmod.h cholmod_ssmult"
  cholmod_ssmult ::
    PtrSparse -> PtrSparse -> SType -> CBool -> CBool -> CMCommon ->
    IO PtrSparse

-- *** Cholesky factorisation routines.
------------------------------------------------------------------------------
foreign import ccall unsafe "suitesparse/cholmod.h cholmod_analyze"
  cholmod_analyse :: PtrSparse -> CMCommon -> IO PtrFactor

foreign import ccall unsafe "suitesparse/cholmod.h cholmod_factorize"
  cholmod_factorise :: PtrSparse -> PtrFactor -> CMCommon -> IO CBool

foreign import ccall unsafe "suitesparse/cholmod.h cholmod_solve"
  cholmod_solve :: SolveMode -> PtrFactor -> PtrDense -> CMCommon -> IO PtrDense

foreign import ccall unsafe "suitesparse/cholmod.h cholmod_solve2"
  cholmod_solve2 ::
    SolveMode -> PtrFactor ->
    PtrDense -> PtrSparse ->         -- right hand side
    Ptr PtrDense -> Ptr PtrSparse -> -- output
    Ptr PtrDense -> Ptr PtrDense  -> -- workspace
    CMCommon -> IO CBool

foreign import ccall unsafe "suitesparse/cholmod.h cholmod_spsolve"
  cholmod_spsolve ::
    SolveMode -> PtrFactor -> PtrSparse -> CMCommon -> IO PtrSparse

foreign import ccall unsafe "suitesparse/cholmod.h cholmod_etree"
  cholmod_etree :: PtrSparse -> Ptr CInt -> CMCommon -> IO CBool


-- ** Factor matrix routines.
------------------------------------------------------------------------------
foreign import ccall unsafe "suitesparse/cholmod.h cholmod_print_factor"
  cholmod_print_factor :: PtrFactor -> CString -> CMCommon -> IO CBool

foreign import ccall unsafe "suitesparse/cholmod.h cholmod_allocate_factor"
  cholmod_allocate_factor :: CSize -> CMCommon -> IO PtrFactor

foreign import ccall unsafe "suitesparse/cholmod.h cholmod_free_factor"
--   cholmod_free_factor :: Ptr PtrFactor -> CMCommon -> IO CBool
  cholmod_free_factor :: Ptr PtrFactor -> CMCommon -> IO ()


-- ** Dense matrix routines.
------------------------------------------------------------------------------
-- | Read a dense matrix from a file.
foreign import ccall unsafe "suitesparse/cholmod.h cholmod_read_dense"
  cholmod_read_dense :: FILE -> CMCommon -> IO PtrDense

foreign import ccall unsafe "suitesparse/cholmod.h cholmod_print_dense"
  cholmod_print_dense :: PtrDense -> CString -> CMCommon -> IO CBool

foreign import ccall unsafe "suitesparse/cholmod.h cholmod_allocate_dense"
  cholmod_allocate_dense ::
    CSize -> CSize -> CSize -> XType -> CMCommon -> IO PtrDense

foreign import ccall unsafe "suitesparse/cholmod.h cholmod_free_dense"
--   cholmod_free_dense :: Ptr PtrDense -> CMCommon -> IO CBool
  cholmod_free_dense :: Ptr PtrDense -> CMCommon -> IO ()

foreign import ccall unsafe "suitesparse/cholmod.h cholmod_ones"
  cholmod_ones :: CSize -> CSize -> XType -> CMCommon -> IO PtrDense

foreign import ccall unsafe "suitesparse/cholmod.h cholmod_sparse_to_dense"
  cholmod_sparse_to_dense :: PtrSparse -> CMCommon -> IO PtrDense


-- ** Higher-level bindings.
------------------------------------------------------------------------------
-- | Ordinary "array" transpose.
transpose' :: MonadIO m => PtrSparse -> CM m PtrSparse
{-# INLINE transpose' #-}
transpose'  = withIO . flip cholmod_transpose values_array

-- | Conjugate transpose.
ctrans' :: MonadIO m => PtrSparse -> CM m PtrSparse
{-# INLINE ctrans' #-}
ctrans'  = withIO . flip cholmod_transpose values_conjugate

symlower' :: MonadIO m => PtrSparse -> CM m PtrSparse
{-# INLINE symlower' #-}
symlower' sp = withIO $ cholmod_copy sp cholmod_s_lower pattern_numerical

symupper' :: MonadIO m => PtrSparse -> CM m PtrSparse
{-# INLINE symupper' #-}
symupper' sp = withIO $ cholmod_copy sp cholmod_s_upper pattern_numerical


-- ** Workspace helper-functions.
------------------------------------------------------------------------------
-- | 
withCM :: (CMCommon -> m a) -> CM m a
{-# INLINE withCM #-}
withCM  = CholModT

-- | 
withIO :: MonadIO m => (CMCommon -> IO a) -> CM m a
{-# INLINE withIO #-}
withIO  = (CholModT $) . (liftIO .)

spfree :: CMCommon -> PtrSparse -> IO ()
{-# INLINE spfree #-}
spfree cm = F.new >=> flip cholmod_free_sparse cm

lffree :: CMCommon -> PtrFactor  -> IO ()
{-# INLINE lffree #-}
lffree cm = F.new >=> flip cholmod_free_factor cm

dmfree :: CMCommon -> PtrDense  -> IO ()
{-# INLINE dmfree #-}
dmfree cm = F.new >=> flip cholmod_free_dense cm
