{-# LANGUAGE BangPatterns, RecordWildCards #-}
{-# LANGUAGE CPP, ForeignFunctionInterface #-}
{-# LANGUAGE Rank2Types, ScopedTypeVariables #-}
module Data.Matrix.CholMod
       ( --CHOLMOD types:
         CholModT(..), CM
       , CMCommon, CMSparse, CMDense, CMFactor, CMSolve(..)
       , SType, cholmod_s_unsym, cholmod_s_lower, cholmod_s_upper
       , IType, cholmod_int, cholmod_long
       , XType, cholmod_real, cholmod_complex
       , DType, cholmod_double, cholmod_single
         -- CHOLMOD workspace functionality:
       , beginCholMod, endCholMod, evalCholMod
       , unsafeCommon, unsafeStepCholMod, unsafeEvalCholMod
         -- Sparse matrix functionality:
       , allocSparse, readSparse, writeSparse, printSparse
       , eyeSparse, copySparse, sparseToDense
       , addSparse, subSparse, mulSparse, mulUpper, aatSparse, addScaleSparse
       , symupper, symlower, transpose, ctrans
       , submatrix, sympermT, symperm
       , setSparseRows, setSparseCols, setSparseCnts, setSparseVals
       , setSparseSorted, setSparseDType
       , getSparseRowN, getSparseColN, getNNZ, getSparseVals
         -- Dense matrix functionality:
--        , allocDense
       , readDense, printDense
       , onesDense
       , getDenseVals, setDenseVals
         -- Factor matrix functionality:
       , printFactor
         -- Cholesky factorisation related routines:
       , analyse, factorise, etree, getFactorPerm
       , solve, solveSystem, solveInplace, solveSparse, solveSparseSystem
       , newSolve, freeSolve, solveResult, solveResult'
       ) where

------------------------------------------------------------------------------
--
--  Operations useful for writing monadic, sparse-matrix code.
--
--  Changelog:
--   + 09/09/2015  --  initial file;
--
--  TODO:
--   + rewrite rules for eliminating steps that just construct foreign-
--     pointers to perform one operation;
--   + can I use (Causal Commutative) Arrows to sequence operations;
--   + bind and figure out how to use `cholmod_solve2` (and `Bset's);
--   + other DSEL ideas, for delayed computations (and somehow use in-place
--     operations, if possible)?
--   + The EIT solver uses a GSM assembly routine, `MakeGSMRaw`, which is
--     much faster than the version that wraps raw pointers with finalisers.
--     Use some way to eliminate the foreign-pointer construction;
--

import Foreign as F
import Foreign.C.Types
import Foreign.C.String
import GHC.ForeignPtr (newConcForeignPtr)
import System.IO.Unsafe
import Control.Monad ((>=>), when)
import Control.Monad.IO.Class
import Data.Vector.Storable (Vector)
import qualified Data.Vector.Storable as Vec
import qualified Data.Vector.Storable.Mutable as Mut
import Data.Vector.Helpers as H
import Text.Printf
import Data.Matrix.Util

import Data.Matrix.CholMod.Bindings

#include <suitesparse/cholmod.h>


-- * CholMod data-structures.
------------------------------------------------------------------------------
-- | CHOLMOD matrix data-structures.
newtype CMSparse = CMSparse { getSparse :: ForeignPtr PtrSparse }
newtype CMDense  = CMDense  { getDense  :: ForeignPtr PtrDense  }
newtype CMFactor = CMFactor { getFactor :: ForeignPtr PtrFactor }

-- | Pointers to some vectors used for the solver's workspace.
data CMSolve = CMSolve { solv'x, solv'y, solv'e :: Ptr PtrDense }


-- ** Sparse matrix wrapping and unwrapping.
------------------------------------------------------------------------------
-- | Unwraps a sparse matrix.
peekSparse :: MonadIO m => CMSparse -> m PtrSparse
{-# INLINE peekSparse #-}
peekSparse cs = liftIO $ peekWith (getSparse cs) return

wrapPtrSparse :: MonadIO m => PtrSparse -> CM m CMSparse
{-# INLINE wrapPtrSparse #-}
wrapPtrSparse  = withIO . flip wrapPtrSparse'

wrapPtrSparse' :: CMCommon -> PtrSparse -> IO CMSparse
{-# INLINE wrapPtrSparse' #-}
wrapPtrSparse' cm sp = do
  p <- F.new sp
  CMSparse <$> newConcForeignPtr p (cholmod_free_sparse p cm)

wrapS :: MonadIO m => (CMCommon -> IO PtrSparse) -> CM m CMSparse
{-# INLINE wrapS #-}
wrapS k = withIO $ \cm -> k cm >>= wrapPtrSparse' cm

-- ** Factor matrix wrapping and unwrapping.
------------------------------------------------------------------------------
-- | Unwraps a factor matrix.
peekFactor :: MonadIO m => CMFactor -> m PtrFactor
{-# INLINE peekFactor #-}
peekFactor cf = liftIO $ peekWith (getFactor cf) return

wrapPtrFactor' :: CMCommon -> PtrFactor -> IO CMFactor
{-# INLINE wrapPtrFactor' #-}
wrapPtrFactor' cm fq = do
  p <- F.new fq
  CMFactor <$> newConcForeignPtr p (cholmod_free_factor p cm)

wrapF :: MonadIO m => (CMCommon -> IO PtrFactor) -> CM m CMFactor
{-# INLINE wrapF #-}
wrapF k = withIO $ \cm -> k cm >>= wrapPtrFactor' cm

-- ** Dense matrix wrapping and unwrapping.
------------------------------------------------------------------------------
-- | Unwraps a dense matrix.
peekDense :: MonadIO m => CMDense -> m PtrDense
{-# INLINE peekDense #-}
peekDense cd = liftIO $ peekWith (getDense cd) return

-- | Dense matrix wrapping.
wrapPtrDense :: MonadIO m => PtrDense -> CM m CMDense
{-# INLINE wrapPtrDense #-}
wrapPtrDense  = withIO . flip wrapPtrDense'

wrapPtrDense' :: CMCommon -> PtrDense -> IO CMDense
{-# INLINE wrapPtrDense' #-}
wrapPtrDense' cm dp = do
  p <- F.new dp
  CMDense <$> newConcForeignPtr p (cholmod_free_dense p cm)

wrapD :: MonadIO m => (CMCommon -> IO PtrDense) -> CM m CMDense
{-# INLINE wrapD #-}
wrapD k = withIO $ \cm -> k cm >>= wrapPtrDense' cm


-- * High-level CHOLMOD functions.
------------------------------------------------------------------------------
-- | Allocate an empty sparse matrix.
allocSparse :: MonadIO m =>
  Int -> Int -> Int -> Bool -> Bool -> SType -> XType -> CM m CMSparse
allocSparse nrow ncol nzmax sorted packed stype xtype =
  wrapS (cholmod_allocate_sparse (fi nrow) (fi ncol) (fi nzmax)
         (toCBool sorted) (toCBool packed) stype xtype)

copySparse :: MonadIO m => CMSparse -> CM m CMSparse
{-# INLINE copySparse #-}
copySparse  = peekSparse >=> wrapS . cholmod_copy_sparse

readSparse :: MonadIO m => FilePath -> CM m CMSparse
readSparse fp = wrapS (readCFile fp . flip cholmod_read_sparse)

writeSparse :: MonadIO m => FilePath -> CMSparse -> CM m ()
writeSparse fp mat = peekSparse mat >>= \sp -> withIO $ \common -> do
  writeCFile fp (\h -> cholmod_write_sparse h sp spnull nullPtr common)
  where spnull = (PtrSparse nullPtr)

printSparse :: MonadIO m => CMSparse -> String -> CM m Bool
printSparse mat name = peekSparse mat >>= \sp -> withIO $ \cm -> do
  label <- newCString name
  fromCBool <$> cholmod_print_sparse sp label cm

eyeSparse :: MonadIO m => Int -> CM m CMSparse
{-# INLINE eyeSparse #-}
eyeSparse n = wrapS $ cholmod_speye m m cholmod_real
  where m = fi n

getNNZ :: MonadIO m => CMSparse -> CM m Int
{-# INLINE getNNZ #-}
-- getNNZ sp = peekSparse sp >>= \p -> withIO (fmap fi . cholmod_nnz p)
getNNZ  = peekSparse >=> (withIO $) . ((fmap fi .) . cholmod_nnz)

transpose :: MonadIO m => CMSparse -> CM m CMSparse
-- {-# INLINE transpose #-}
{-# SPECIALIZE transpose :: CMSparse -> CM IO CMSparse #-}
transpose  = peekSparse >=> transpose' >=> wrapPtrSparse

-- | Conjugate transpose.
ctrans :: MonadIO m => CMSparse -> CM m CMSparse
-- {-# INLINE ctrans #-}
{-# SPECIALIZE ctrans :: CMSparse -> CM IO CMSparse #-}
ctrans  = peekSparse >=> ctrans' >=> wrapPtrSparse

symlower :: MonadIO m => CMSparse -> CM m CMSparse
-- {-# INLINE symlower #-}
{-# SPECIALIZE symlower :: CMSparse -> CM IO CMSparse #-}
symlower  = peekSparse >=> symlower' >=> wrapPtrSparse

symupper :: MonadIO m => CMSparse -> CM m CMSparse
-- {-# INLINE symupper #-}
{-# SPECIALIZE symupper :: CMSparse -> CM IO CMSparse #-}
symupper  = peekSparse >=> symupper' >=> wrapPtrSparse

submatrix :: MonadIO m => CMSparse -> IVec -> IVec -> CM m CMSparse
-- {-# INLINE submatrix #-}
{-# SPECIALIZE submatrix :: CMSparse -> IVec -> IVec -> CM IO CMSparse #-}
submatrix cs ix jx = peekSparse cs >>= wrapS . submatrix' ix jx

submatrix' :: IVec -> IVec -> PtrSparse -> CMCommon -> IO PtrSparse
{-# INLINE submatrix' #-}
submatrix' ix jx ptr cm = do
  ip <- Vec.unsafeWith (Vec.map ic ix) return
  jp <- Vec.unsafeWith (Vec.map ic jx) return
  cholmod_submatrix ptr ip (fi $ len ix) jp (fi $ len jx) ctrue cfalse cm

-- | Symmetric permutation.
--   TODO: This doesn't work?
symperm :: MonadIO m => CMSparse -> IVec -> CM m CMSparse
{-# SPECIALIZE symperm :: CMSparse -> IVec -> CM IO CMSparse #-}
symperm mat p = peekSparse mat >>= wrapS . submatrix' p p

-- | Symmetric permutation and transpose.
sympermT :: MonadIO m => CMSparse -> Vector Int -> CM m CMSparse
{-# SPECIALIZE sympermT :: CMSparse -> IVec -> CM IO CMSparse #-}
sympermT mat perm = do
  sp <- peekSparse mat
  pp <- peekVector (Vec.map ic perm)
  wrapS (cholmod_ptranspose sp values_array pp pp (fi $ len perm))

------------------------------------------------------------------------------
-- | Add two sparse matrices.
addSparse :: MonadIO m => CMSparse -> CMSparse -> CM m CMSparse
{-# INLINE addSparse #-}
addSparse aa bb = do
  p <- peekSparse aa
  q <- peekSparse bb
  wrapS (cholmod_add p q twoOnes twoOnes ctrue cfalse)

subSparse :: MonadIO m => CMSparse -> CMSparse -> CM m CMSparse
{-# INLINE subSparse #-}
subSparse aa bb = do
  p <- peekSparse aa
  q <- peekSparse bb
  wrapS (cholmod_add p q twoOnes twoMinusOnes ctrue cfalse)

-- | Scale matrices A & B, and then add.
addScaleSparse ::
  MonadIO m => CMSparse -> CMSparse -> Double -> Double -> CM m CMSparse
{-# INLINE addScaleSparse #-}
addScaleSparse aa bb alpha beta = do
  p <- peekSparse aa
  q <- peekSparse bb
  r <- liftIO $ poke2 alpha 0
  s <- liftIO $ poke2 beta  0
  mat <- wrapS (cholmod_add p q r s ctrue cfalse)
  liftIO $ free r >> free s >> return mat

-- | Multiply, computing numerical values, but without sorting the product-
--   matrix row-indices.
mulSparse :: MonadIO m => CMSparse -> CMSparse -> CM m CMSparse
{-# INLINE mulSparse #-}
mulSparse aa bb = do
  p <- peekSparse aa
  q <- peekSparse bb
  wrapS (cholmod_ssmult p q cholmod_s_unsym ctrue cfalse)

mulUpper :: MonadIO m => CMSparse -> CMSparse -> CM m CMSparse
{-# INLINE mulUpper #-}
mulUpper aa bb = do
  p <- peekSparse aa
  q <- peekSparse bb
  wrapS (cholmod_ssmult p q cholmod_s_upper ctrue cfalse)

-- | Sparse-matrix operation: A*A'
aatSparse :: MonadIO m => CMSparse -> IVec -> Int -> Pattern -> CM m CMSparse
{-# INLINE aatSparse #-}
aatSparse aa fset fnum mode = do
  p <- peekSparse aa
  q <- peekVector (Vec.map ic fset)
  wrapS (cholmod_aat p q (fi fnum) mode)

------------------------------------------------------------------------------
-- | Compute the shape of the Cholesky lower-factor, for the given matrix.
analyse :: MonadIO m => CMSparse -> CM m CMFactor
{-# INLINE analyse #-}
analyse aa = peekSparse aa >>= wrapF . cholmod_analyse

factorise :: MonadIO m => CMSparse -> CMFactor -> CM m Bool
{-# INLINE factorise #-}
factorise aa ll = do
  sp <- peekSparse aa
  lq <- peekFactor ll
  withIO $ fmap fromCBool . cholmod_factorise sp lq

etree :: MonadIO m => CMSparse -> CM m (Vector CInt)
etree aa = peekSparse aa >>= \sp -> withIO $ \common -> do
  ncol <- #{peek cholmod_sparse, ncol} (ptrSparse sp)
  H.new ncol >>= \ar ->
    Mut.unsafeWith ar (flip (cholmod_etree sp) common) >> frz ar

solve :: MonadIO m => CMFactor -> CMDense -> CM m CMDense
{-# INLINE solve #-}
solve  = solveSystem solve_A

solveSystem :: MonadIO m => SolveMode -> CMFactor -> CMDense -> CM m CMDense
{-# INLINE solveSystem #-}
solveSystem m ll bb = do
  fq <- peekFactor ll
  dp <- peekDense  bb
  wrapD $ cholmod_solve m fq dp

solveSparse :: MonadIO m => CMFactor -> CMSparse -> CM m CMSparse
{-# INLINE solveSparse #-}
solveSparse  = solveSparseSystem solve_A

solveSparseSystem ::
  MonadIO m => SolveMode -> CMFactor -> CMSparse -> CM m CMSparse
{-# INLINE solveSparseSystem #-}
solveSparseSystem m ll bb = do
  fq <- peekFactor ll
  sp <- peekSparse bb
  wrapS $ cholmod_spsolve m fq sp

------------------------------------------------------------------------------
-- | For reusing memory, and using sparse `Bset's.
--   TODO: Currently no faster?
newSolve :: MonadIO m => m CMSolve
{-# INLINE newSolve #-}
newSolve  = liftIO $ CMSolve <$> F.new dnull <*> F.new dnull <*> F.new dnull
  where
    dnull = PtrDense nullPtr

freeSolve :: MonadIO m => CMSolve -> CM m ()
{-# SPECIALIZE freeSolve :: CMSolve -> CM IO () #-}
freeSolve (CMSolve px py pe) = CholModT $ \cm -> liftIO $ do
  peek px >>= dmfree cm >> peek py >>= dmfree cm >> peek pe >>= dmfree cm

solveResult :: MonadIO m => CMSolve -> CM m CMDense
{-# INLINE solveResult #-}
solveResult (CMSolve x _ _) = liftIO (peek x) >>= wrapPtrDense

-- | Just peek at the result, without copying.
solveResult' :: MonadIO m => CMSolve -> CM m (Vector Double)
{-# INLINE solveResult' #-}
solveResult' (CMSolve x _ _) = liftIO $ peek x >>= unsafePeekDenseVals'

solveInplace ::
  MonadIO m => CMFactor -> CMDense -> CMSolve -> CM m (Vector Double)
{-# INLINE solveInplace #-}
solveInplace matL bvec (CMSolve px py pe) = do
  mp <- peekFactor matL
  bq <- peekDense  bvec
  withIO $ cholmod_solve2 solve_A mp bq (PtrSparse nullPtr) px nullPtr py pe
  liftIO $ peek px >>= unsafePeekDenseVals'


-- ** Factor matrix user API.
------------------------------------------------------------------------------
printFactor :: MonadIO m => CMFactor -> String -> CM m Bool
printFactor (CMFactor ptr) name = CholModT $ \common -> liftIO $ do
  label <- newCString name
  peekWith ptr $ \matrix ->
    fromCBool <$> cholmod_print_factor matrix label common

getFactorPerm :: MonadIO m => CMFactor -> CM m (Vector Int)
getFactorPerm (CMFactor ptr) = liftIO $ do
  peekWith ptr $ \(PtrFactor pfac) -> do
    n <- #{peek cholmod_factor, n} pfac :: IO CSize
    p <- #{peek cholmod_factor, Perm} pfac :: IO (Ptr CInt)
    Vec.map ci <$> vcopy (fi n) p


-- ** Dense matrix user API.
------------------------------------------------------------------------------
-- | Read a dense matrix from the given file.
readDense :: MonadIO m => FilePath -> CM m CMDense
{-# SPECIALIZE readDense :: FilePath -> CM IO CMDense #-}
readDense fp = CholModT $ \cm -> liftIO $ do
  readCFile fp (flip cholmod_read_dense cm) >>= wrapPtrDense' cm

printDense :: MonadIO m => CMDense -> String -> CM m Bool
{-# SPECIALIZE printDense :: CMDense -> String -> CM IO Bool #-}
printDense (CMDense ptr) name = CholModT $ \common -> liftIO $ do
  label <- newCString name
  peekWith ptr $ \matrix ->
    fromCBool <$> cholmod_print_dense matrix label common

-- | Construct a dense matrix full of ones.
onesDense :: MonadIO m => Int -> Int -> CM m CMDense
{-# SPECIALIZE onesDense :: Int -> Int -> CM IO CMDense #-}
onesDense nrow ncol = wrapD (cholmod_ones (fi nrow) (fi ncol) cholmod_real)

sparseToDense :: MonadIO m => CMSparse -> CM m CMDense
{-# SPECIALIZE sparseToDense :: CMSparse -> CM IO CMDense #-}
sparseToDense mat = peekSparse mat >>= wrapD . cholmod_sparse_to_dense

getDenseVals :: MonadIO m => CMDense -> CM m (Vector Double)
{-# INLINE getDenseVals #-}
getDenseVals mat = liftIO $ peekDense mat >>= \(PtrDense p) -> do
  nz <- #{peek cholmod_dense, nzmax} p
  xp <- #{peek cholmod_dense, x} p
  vcopy nz xp

setDenseVals :: MonadIO m => DVec -> CMDense -> CM m ()
{-# INLINE setDenseVals #-}
setDenseVals vs mat = liftIO $ peekDense mat >>= \(PtrDense p) -> do
  nz <- #{peek cholmod_dense, nzmax} p
  xp <- #{peek cholmod_dense, x} p
  Vec.unsafeWith vs $ \q -> copyArray xp q nz

-- TODO: Doesn't really belong here?
unsafePeekDenseVals' :: PtrDense -> IO (Vector Double)
{-# INLINE unsafePeekDenseVals' #-}
unsafePeekDenseVals' (PtrDense p) = do
  nz <- #{peek cholmod_dense, nzmax} p
  xp <- #{peek cholmod_dense, x} p
  flip Vec.unsafeFromForeignPtr0 nz <$> newForeignPtr_ xp


-- * CholMod Workspace functions.
------------------------------------------------------------------------------
-- | CholMod error-handler callback.
--   TODO: Somehow close an open file.
type Handler = CInt -> CString -> CInt -> CString -> IO ()

foreign export ccall cholmod_file_error_handler :: Handler
foreign import ccall "wrapper" mkHandler :: Handler -> IO (FunPtr Handler)

cholmod_file_error_handler :: CInt -> CString -> CInt -> CString -> IO ()
cholmod_file_error_handler status file line msg = do
  file'   <- peekCString file
  message <- peekCString msg
  printf "cholmod error: file: %s line: %d status: %d: %s\n"
    file' (fi line :: Int) (fi status :: Int) message
  if status < 0 then exit 2 else exit 1

-- | Create the CholMod workspace.
beginCholMod :: IO (Maybe CMCommon)
beginCholMod  = do
  p <- mallocBytes #{size cholmod_common}
  let cm = CMCommon p
  fromCBool <$> cholmod_start cm >>= \b -> case b of
    True  -> do
      mkHandler cholmod_file_error_handler >>= \eh -> do
        #{poke cholmod_common, error_handler} p eh
        freeHaskellFunPtr eh
      return $ Just cm
    False -> return Nothing

-- | Close a CholMod session.
endCholMod :: CMCommon -> IO Bool
endCholMod  = fmap fromCBool . cholmod_finish

evalCholMod :: MonadIO m => CholModT m a -> m a
evalCholMod m = do
  mcm <- liftIO $ beginCholMod
  let go cm = runCholModT m cm >>= \x -> liftIO (cholmod_finish cm) >> return x
  maybe (error "cholmod common error\n") go mcm

unsafeEvalCholMod :: CM IO a -> a
unsafeEvalCholMod  = unsafePerformIO . evalCholMod

unsafeStepCholMod :: CM IO a -> CMCommon -> a
unsafeStepCholMod m = unsafePerformIO . runCholModT m

unsafeCommon :: CMCommon
unsafeCommon  = unsafePerformIO $ do
  cm <- CMCommon <$> mallocBytes #{size cholmod_common}
  _  <- cholmod_start cm
  mkHandler cholmod_file_error_handler >>= \eh -> do
    #{poke cholmod_common, error_handler} (ptrCMCommon cm) eh
    freeHaskellFunPtr eh
  return cm


-- * Sparse matrix modification.
------------------------------------------------------------------------------
-- | Set the row-indices of the given sparse matrix.
setSparseRows :: MonadIO m => Vector Int -> CMSparse -> CM m ()
{-# INLINE setSparseRows #-}
setSparseRows ri mat = liftIO $ do
  PtrSparse p <- peekSparse mat
  nz <- #{peek cholmod_sparse, nzmax} p
  ip <- #{peek cholmod_sparse, i} p
  when (len ri == nz) $ fromv ip nz (Vec.map ic ri)

-- | Set the non-zero elements of the given sparse matrix.
--   FIXME: Currently only works for doubles.
setSparseVals :: (Storable a, MonadIO m) => Vector a -> CMSparse -> CM m ()
{-# INLINE setSparseVals #-}
setSparseVals xs mat = liftIO $ do
  PtrSparse p <- peekSparse mat
  nz <- #{peek cholmod_sparse, nzmax} p
  xp <- #{peek cholmod_sparse, x} p
  when (len xs == nz) $ fromv xp nz xs

-- | Set the column-pointers of the given sparse-matrix, and set the matrix-
--   mode to packed.
setSparseCols :: MonadIO m => Vector Int -> CMSparse -> CM m ()
{-# INLINE setSparseCols #-}
setSparseCols cp mat = liftIO $ do
  PtrSparse p <- peekSparse mat
  cc <- (+1) <$> #{peek cholmod_sparse, ncol} p
  pc <- #{peek cholmod_sparse, p} p
  when (len cp == cc) $
    fromv pc cc (Vec.map ic cp) >> #{poke cholmod_sparse, packed} p ctrue

setSparseCnts :: MonadIO m => Vector Int -> CMSparse -> CM m ()
{-# INLINE setSparseCnts #-}
setSparseCnts cp mat = liftIO $ do
  PtrSparse p <- peekSparse mat
  let cs = Vec.map ic $ Vec.zipWith (-) (Vec.unsafeTail cp) cp
  cn <- #{peek cholmod_sparse, ncol} p
  pn <- #{peek cholmod_sparse, nz  } p
  when (len cs == cn) $
    fromv pn cn cs >> #{poke cholmod_sparse, packed} p cfalse

setSparseDType :: MonadIO m => DType -> CMSparse -> CM m ()
{-# INLINE setSparseDType #-}
setSparseDType dtype (CMSparse ptr) =
  liftIO $ peekWith ptr $ flip #{poke cholmod_sparse, dtype} dtype . ptrSparse

setSparseSorted :: MonadIO m => Bool -> CMSparse -> CM m ()
{-# INLINE setSparseSorted #-}
setSparseSorted sorted mat = withIO $ \cm -> do
  ptr <- peekSparse mat
  #{poke cholmod_sparse, sorted} (ptrSparse ptr) (toCBool sorted)
  when sorted $ cholmod_sort ptr cm >> return ()

getSparseVals :: MonadIO m => CMSparse -> CM m (Vector Double)
{-# INLINE getSparseVals #-}
getSparseVals mat = liftIO $ do
  PtrSparse p <- peekSparse mat
  nz <- #{peek cholmod_sparse, nzmax} p
  #{peek cholmod_sparse, x} p >>= vcopy nz

getSparseRowN :: MonadIO m => CMSparse -> CM m Int
{-# INLINE getSparseRowN #-}
getSparseRowN (CMSparse ptr) =
  liftIO $ peekWith ptr $ fmap ci . #{peek cholmod_sparse, nrow} . ptrSparse

getSparseColN :: MonadIO m => CMSparse -> CM m Int
{-# INLINE getSparseColN #-}
getSparseColN (CMSparse ptr) =
  liftIO $ peekWith ptr $ fmap ci . #{peek cholmod_sparse, ncol} . ptrSparse


-- * Helper functions.
------------------------------------------------------------------------------
twoOnes, twoMinusOnes :: Ptr Double
twoOnes      = unsafePerformIO $ poke2 1 1
twoMinusOnes = unsafePerformIO $ poke2 (-1) (-1)

poke2 :: forall a. Storable a => a -> a -> IO (Ptr a)
{-# INLINE poke2 #-}
poke2 a b = mallocBytes (2*sizeOf a) >>= \p ->
  poke p a >> pokeElemOff p 1 b >> return p
