{-# LANGUAGE Rank2Types #-}
module Data.Vector.Helpers where

------------------------------------------------------------------------------
--
--  Operations useful for writing monadic, sparse-matrix code.
--
--  Changelog:
--   + 04/09/2015  --  initial file;
--

import Control.Monad.Primitive
import Data.Vector.Storable (Vector)
import Data.Vector.Storable.Mutable (MVector)
import qualified Data.Vector.Generic as G
import qualified Data.Vector.Generic.Mutable as M


-- * Some convenient types.
------------------------------------------------------------------------------
-- | Convenience aliases for some vector-types.
type IVec = Vector Int
type DVec = Vector Double
type BVec = Vector Bool

type MVec  m a = MVector (PrimState m) a
type MVecD m   = MVector (PrimState m) Double
type MVecI m   = MVector (PrimState m) Int


(!) :: G.Vector v a => v a -> Int -> a
{-# INLINE (!) #-}
(!)  = G.unsafeIndex

len :: G.Vector v a => v a -> Int
{-# INLINE len #-}
len  = G.length

vec :: G.Vector v a => [a] -> v a
{-# INLINE vec #-}
vec  = G.fromList

frz :: forall m v a. (PrimMonad m, G.Vector v a) =>
       G.Mutable v (PrimState m) a -> m (v a)
{-# INLINE frz #-}
-- frz  = G.freeze
frz  = G.unsafeFreeze

thw :: forall m v a. (PrimMonad m, G.Vector v a) =>
       v a -> m (G.Mutable v (PrimState m) a)
{-# INLINE thw #-}
-- thw  = G.thaw
thw  = G.unsafeThaw

slc :: G.Vector v a => Int -> Int -> v a -> v a
{-# INLINE slc #-}
slc  = G.unsafeSlice

tak :: G.Vector v a => Int -> v a -> v a
{-# INLINE tak #-}
tak  = G.unsafeTake

drp :: G.Vector v a => Int -> v a -> v a
{-# INLINE drp #-}
drp  = G.unsafeDrop

lst :: G.Vector v a => v a -> a
{-# INLINE lst #-}
lst  = G.unsafeLast

new :: (PrimMonad m, M.MVector v a) => Int -> m (v (PrimState m) a)
{-# INLINE new #-}
new  = M.unsafeNew

rd :: (PrimMonad m, M.MVector v a) => v (PrimState m) a -> Int -> m a
{-# INLINE rd #-}
rd  = M.unsafeRead

wr :: (PrimMonad m, M.MVector v a) => v (PrimState m) a -> Int -> a -> m ()
{-# INLINE wr #-}
wr  = M.unsafeWrite
