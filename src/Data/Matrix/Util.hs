{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE CPP, ForeignFunctionInterface #-}
{-# LANGUAGE Rank2Types, ScopedTypeVariables #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Data.Matrix.Util where

------------------------------------------------------------------------------
--
--  Bindings to the `CSparse` concise sparse-matrix library.
--
--  Changelog:
--   + 09/09/2015  --  initial file;
--

-- import GHC.Types (SPEC(..))
import Foreign
import Foreign.C.Types
import Foreign.C.String
import System.Exit
import Control.Monad ((>=>))
import Control.Monad.IO.Class
import Data.Vector.Storable (Vector)
import qualified Data.Vector.Storable as Vec
import qualified Data.Vector.Storable.Mutable as Mut
import Data.Vector.Helpers as H


-- * Standard C-library data-types.
------------------------------------------------------------------------------
newtype FILE = FILE { unFILE :: Ptr FILE } deriving (Eq, Show, Storable)


-- * Standard C-library functions.
------------------------------------------------------------------------------
-- | Standard C file-I/O functions.
foreign import ccall unsafe "stdio.h fopen"  fopen  :: CString -> CString -> IO FILE
foreign import ccall unsafe "stdio.h fclose" fclose :: FILE -> IO ()

-- | Open a file and execute the given action, using the C file handle.
withCFile :: MonadIO m => String -> FilePath -> (FILE -> m a) -> m a
withCFile mode name action = do
  filename <- liftIO $ newCString name
  filemode <- liftIO $ newCString mode
  file     <- liftIO $ fopen filename filemode
  result   <- action file
  liftIO $ fclose file
  return result

-- | Open a file and execute the given action, using the C file handle.
readCFile :: MonadIO m => FilePath -> (FILE -> m a) -> m a
{-# INLINE readCFile #-}
readCFile  = withCFile "r"

-- | Open a file and execute the given action, using the C file handle.
writeCFile :: MonadIO m => FilePath -> (FILE -> m a) -> m a
{-# INLINE writeCFile #-}
writeCFile  = withCFile "w"


-- * Additional foreign function interface routines.
------------------------------------------------------------------------------
peekWith :: Storable a => ForeignPtr a -> (a -> IO b) -> IO b
{-# INLINE peekWith #-}
peekWith ptr action = withForeignPtr ptr $ peek >=> action

peekVector :: (Storable a, MonadIO m) => Vector a -> m (Ptr a)
{-# INLINE peekVector #-}
peekVector vs = liftIO $ Vec.unsafeWith vs return


-- * Vector and memory operations.
------------------------------------------------------------------------------
-- | Copy the contents of a pointer into a Haskell Storable Vector.
--   NOTE: The different versions all behave slightly differently.
vcopy :: forall a. Storable a => Int -> Ptr a -> IO (Vector a)
{-# INLINE vcopy #-}
vcopy n p =
  H.new n >>= \ar -> Mut.unsafeWith ar (\q -> copyArray q p n) >> frz ar

-- vcopy n p = mallocArray n >>= \q -> do
--   copyArray q p n
--   flip Vec.unsafeFromForeignPtr0 n <$> newForeignPtr finalizerFree q

-- vcopy n p = mallocForeignPtrBytes (n*sizeOf (undefined :: a)) >>= \q -> do
-- -- vcopy n p = mallocForeignPtrArray n >>= \q -> do
--   withForeignPtr q (\t -> copyArray t p n)
--   return $ Vec.unsafeFromForeignPtr0 q n

-- | Copy the contents of a `Vector` into a new array, then store the pointer
--   to the array.
--   NOTE: Pointless, because the contents of a `Vector` don't change (unless
--     unsafe operations have occured).
copyv :: Storable a => Vector a -> Ptr (Ptr a) -> IO ()
{-# INLINE copyv #-}
copyv vs p = do
  let n = Vec.length vs
  q <- mallocArray n
  Vec.unsafeWith vs $ \t -> copyArray q t n
  poke p q

------------------------------------------------------------------------------
-- | Construct a `Vector` using the given pointer.
vmake :: Storable a => Int -> Ptr a -> IO (Vector a)
{-# INLINE vmake #-}
-- vmake n p = flip Vec.unsafeFromForeignPtr0 n <$> newForeignPtr finalizerFree p
vmake n p = flip Vec.unsafeFromForeignPtr0 n <$> newForeignPtr_ p

-- | Set `n` elements of the array, pointed to by `p`, and fill with the
--   contents of `xs`.
fromv :: Storable a => Ptr a -> Int -> Vector a -> IO ()
{-# INLINE fromv #-}
fromv p n = flip Vec.unsafeWith (flip (copyArray p) n)

-- NOTE: Unsafe, as it overwrites a frozen vector.
tov :: Storable a => Ptr a -> Int -> Vector a -> IO ()
{-# INLINE tov #-}
tov p n = flip Vec.unsafeWith (\q -> copyArray q p n)


-- * Convenience conversions, between various integral representations.
------------------------------------------------------------------------------
fi :: (Integral i, Num a) => i -> a
{-# INLINE fi #-}
fi  = fromIntegral

ci :: CInt -> Int
{-# INLINE ci #-}
ci  = fromIntegral

ic :: Int -> CInt
{-# INLINE ic #-}
ic  = fromIntegral


-- * Pointer functions.
------------------------------------------------------------------------------
-- | Pointer arithmetic, and without recasting.
infixl 6 +>

(+>) :: Ptr a -> Int -> Ptr a
{-# INLINE (+>) #-}
(+>)  = plusPtr

bptr :: Ptr a -> Ptr Word8
{-# INLINE bptr #-}
bptr  = castPtr


-- * System functions.
------------------------------------------------------------------------------
exit :: Int -> IO a
{-# INLINE exit #-}
exit  = exitWith . ExitFailure
