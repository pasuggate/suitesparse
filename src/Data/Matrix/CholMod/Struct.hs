{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE CPP, ForeignFunctionInterface #-}
{-# LANGUAGE Rank2Types, ScopedTypeVariables #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RecordWildCards #-}
module Data.Matrix.CholMod.Struct where

------------------------------------------------------------------------------
--
--  Some data-structures for manipulating CHOLMOD's C-structs.
--
--  Changelog:
--   + 11/09/2015  --  initial file;
--


------------------------------------------------------------------------------
-- | Sparse matrices that are managed via CholMod.
newtype CMSparse =
  CMSparse { ptrCMSparseType :: Ptr (CMSparseType Word8) }
  deriving (Eq, Show, Storable)


------------------------------------------------------------------------------
-- | CholMod's sparse-matrix data-structure.
data CMSparseType a = CMSparseType
  { cms'nrow   :: Int
  , cms'ncol   :: Int
  , cms'nzmax  :: Int
  , cms'p      :: Vector Int
  , cms'i      :: Vector Int
  , cms'nz     :: Vector Int        -- NNZ per col.
  , cms'x      :: Vector a
  , cms'z      :: Vector a
  , cms'stype  :: SType
  , cms'itype  :: IType             -- Determines the index & pointer types
  , cms'xtype  :: XType
  , cms'dtype  :: DType             -- Are `x` & `z` double or float?
  , cms'sorted :: Bool
  , cms'packed :: Bool              -- If packed, then `nz` is ignored
  } deriving (Eq, Show)

type CMSparseF = CMSparseType Float
type CMSparseD = CMSparseType Double
-- type CMSparseC = CMSparseType (Complex Double) -- TODO?

------------------------------------------------------------------------------
-- | CholMod's dense-matrix data-structure.
data CMDenseType a = CMDenseType
  { cmd'nrow   :: Int
  , cmd'ncol   :: Int
  , cmd'nzmax  :: Int
  , cmd'd      :: Int           -- the leading dimension
  , cmd'x      :: Vector a
  , cmd'z      :: Vector a
  , cmd'xtype  :: XType
  , cmd'dtype  :: DType         -- are `x` & `z` double or float?
  } deriving (Eq, Show)

type CMDenseF = CMDenseType Float
type CMDenseD = CMDenseType Double


-- * Storable instances, for the bindings.
------------------------------------------------------------------------------
{--}
-- TODO: Not useful? Can be derived automatically?
instance Storable CMCommon where
  sizeOf  _ = #{size cholmod_common}
  alignment = const (sizeOf (undefined :: Ptr ()))
  peek p    = do
    q <- mallocBytes #{size cholmod_common}
    copyArray (bptr q) (bptr p) #{size cholmod_common}
    return $ CMCommon q
  poke p (CMCommon q) = do
    copyArray (bptr p) (bptr q) #{size cholmod_common}
--}

-- TODO: Check that the matrix-type values make sense.
instance Storable (SparsePtrType Double) where
  {-# INLINE sizeOf #-}
  sizeOf  _ = #{size cholmod_sparse}
  {-# INLINE alignment #-}
  alignment = const (sizeOf (undefined :: Ptr ()))
  peek p    = do
    nrow   <- ci <$> #{peek cholmod_sparse, nrow} p
    ncol   <- ci <$> #{peek cholmod_sparse, ncol} p
    nzmax  <- ci <$> #{peek cholmod_sparse, nzmax} p
    cp     <- vcopy (ncol+1) =<< #{peek cholmod_sparse, p} p
    ri     <- vcopy  nzmax   =<< #{peek cholmod_sparse, i} p
    nz     <- vcopy  ncol    =<< #{peek cholmod_sparse, nz} p
    xs     <- vcopy  nzmax   =<< #{peek cholmod_sparse, x} p
    zs     <- return mempty
    stype  <- #{peek cholmod_sparse, stype} p
    itype  <- #{peek cholmod_sparse, itype} p
    xtype  <- #{peek cholmod_sparse, xtype} p
    dtype  <- #{peek cholmod_sparse, dtype} p
    sorted <- fromCBool <$> #{peek cholmod_sparse, sorted} p
    packed <- fromCBool <$> #{peek cholmod_sparse, sorted} p
    return $ SparsePtrType nrow ncol nzmax cp ri nz xs zs stype itype xtype dtype sorted packed
  poke p SparsePtrType{..} = do
    #{poke cholmod_sparse, nrow  } p $ ic cms'nrow
    #{poke cholmod_sparse, ncol  } p $ ic cms'ncol
    #{poke cholmod_sparse, nzmax } p $ ic cms'nzmax
    Vec.unsafeWith cms'p  $ #{poke cholmod_sparse, p } p
    Vec.unsafeWith cms'i  $ #{poke cholmod_sparse, i } p
    Vec.unsafeWith cms'nz $ #{poke cholmod_sparse, nz} p
    Vec.unsafeWith cms'x  $ #{poke cholmod_sparse, x } p
    #{poke cholmod_sparse, z     } p nullPtr
    #{poke cholmod_sparse, stype } p cms'stype
    #{poke cholmod_sparse, itype } p cms'itype
    #{poke cholmod_sparse, xtype } p cms'xtype
    #{poke cholmod_sparse, dtype } p cms'dtype
    #{poke cholmod_sparse, sorted} p cms'sorted
    #{poke cholmod_sparse, packed} p cms'packed
