{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE CPP, ForeignFunctionInterface #-}
{-# LANGUAGE Rank2Types, ScopedTypeVariables #-}
module Data.Matrix.CXSparse.Legacy where

------------------------------------------------------------------------------
--
--  Bindings to the `CXSparse` concise sparse-matrix library.
--
--  Changelog:
--   + 04/09/2015  --  initial file;
--

-- import GHC.Types (SPEC(..))
-- import GHC.Int
import Foreign as F
import Foreign.C.Types
import System.IO.Unsafe
import Control.Monad
import Data.Bool
import Data.Vector.Storable (Vector)
import qualified Data.Vector.Storable as Vec
-- import qualified Data.Vector.Generic as G
-- import Data.Vector.Helpers as H
import Data.Matrix.Util

#include <suitesparse/cs.h>
-- #include <suitesparse/cxsparse/cs.h>


-- * Concise Sparse-matrix library data-structures.
------------------------------------------------------------------------------
data CXSparse = CXSparse
  { cs'nzmax :: Int
  , cs'rowN  :: Int
  , cs'colN  :: Int
  , cs'colp  :: Vector Int
  , cs'rowi  :: Vector Int
  , cs'xs    :: Vector Double
  , cs'nz    :: Maybe Int
  } deriving (Eq, Show)


-- * Some instances for the data-structures.
------------------------------------------------------------------------------
instance Storable CXSparse where
  {-# INLINE sizeOf #-}
  sizeOf  _ = #{size cs_dl}
  {-# INLINE alignment #-}
  alignment = const (sizeOf (undefined :: Ptr ()))
  -- NOTE: Copies the arrays on `peek`.
  peek p = do
    nzm <- #{peek cs_dl, nzmax} p
    m   <- #{peek cs_dl, m} p
    n   <- #{peek cs_dl, m} p
    nz  <- #{peek cs_dl, nz} p
    let mnz = bool (Just nz) Nothing (nz == (-1))
    cs  <- #{peek cs_dl, p} p >>= vcopy (maybe (n+1) id mnz)
    rs  <- #{peek cs_dl, i} p >>= vcopy nzm
    xs  <- #{peek cs_dl, x} p >>= vcopy nzm
    return $ CXSparse nzm m n cs rs xs mnz
  -- TODO: Should the vector contents be copied out?
  poke p (CXSparse nzmax m n cs rs xs mnz) = do
    #{poke cs_dl, nzmax} p nzmax
    #{poke cs_dl, m} p m
    #{poke cs_dl, n} p n
    cs `Vec.unsafeWith` #{poke cs_dl, p} p
    rs `Vec.unsafeWith` #{poke cs_dl, i} p
    xs `Vec.unsafeWith` #{poke cs_dl, x} p
    #{poke cs_dl, nz} p (maybe (-1) id mnz)


-- * Foreign bindings.
------------------------------------------------------------------------------
foreign import ccall unsafe "suitesparse/cs.h cs_spfree"
  cs_spfree :: Ptr CXSparse -> IO ()

foreign import ccall unsafe "suitesparse/cs.h cs_free"
  cs_free :: Ptr a -> IO ()

foreign import ccall unsafe "suitesparse/cs.h &cs_free"
  p_cs_free :: FunPtr (Ptr a -> IO ())

foreign import ccall unsafe "suitesparse/cs.h &cs_spfree"
  p_cs_spfree :: FunPtr (Ptr CXSparse -> IO ())

foreign import ccall unsafe "suitesparse/cs.h cs_add"
  cs_add :: Ptr CXSparse -> Ptr CXSparse -> CDouble -> CDouble -> Ptr CXSparse

foreign import ccall unsafe "suitesparse/cs.h cs_multiply"
  cs_multiply :: Ptr CXSparse -> Ptr CXSparse -> Ptr CXSparse

foreign import ccall unsafe "suitesparse/cs.h cs_transpose"
  cs_transpose :: Ptr CXSparse -> CInt -> Ptr CXSparse


-- * Bound CXSparse API functions.
------------------------------------------------------------------------------
-- | Copies the given matrix.
cs'copy :: CXSparse -> CXSparse
cs'copy cs = unsafePerformIO $ F.new cs >>= peek

-- | First scale the two given matrices, then add them.
cs'add :: CXSparse -> CXSparse -> Double -> Double -> CXSparse
cs'add a b alpha beta = unsafePerformIO $ do
  pa <- F.new a
  pb <- F.new b
  cspeek $ cs_add pa pb (realToFrac alpha) (realToFrac beta)

cs'multiply :: CXSparse -> CXSparse -> CXSparse
cs'multiply a b =
  unsafePerformIO $ cs_multiply <$> F.new a <*> F.new b >>= cspeek

cs'transpose :: CXSparse -> Bool -> CXSparse
cs'transpose m vals = unsafePerformIO $ do
  p <- F.new m
  cspeek $ cs_transpose p (bool 0 1 vals)


-- * Helper functions.
------------------------------------------------------------------------------
-- | Read a CXSparse matrix, from a C-allocated pointer, then free the matrix.
--   NOTE: Copies all data into the GHC-managed heap.
cspeek :: Ptr CXSparse -> IO CXSparse
{-# INLINE cspeek #-}
cspeek p = peek p >>= \m -> cs_spfree p >> return m
-- cspeek p = foreignPeek p >>= \m -> cs_free p >> return m

-- | Peek a `CXSparse`, but without copying the data.
foreignPeek :: Ptr CXSparse -> IO CXSparse
{-# INLINE foreignPeek #-}
foreignPeek p = do
  nzm <- #{peek cs_dl, nzmax} p
  m   <- #{peek cs_dl, m} p
  n   <- #{peek cs_dl, m} p
  nz  <- #{peek cs_dl, nz} p
  let mnz = bool (Just nz) Nothing (nz == (-1))
  cs  <- #{peek cs_dl, p} p >>= vpeek (maybe (n+1) id mnz)
  rs  <- #{peek cs_dl, i} p >>= vpeek nzm
  xs  <- #{peek cs_dl, x} p >>= vpeek nzm
--   cs  <- #{peek cs_dl, p} p >>= vmake (maybe (n+1) id mnz)
--   rs  <- #{peek cs_dl, i} p >>= vmake nzm
--   xs  <- #{peek cs_dl, x} p >>= vmake nzm
  return $ CXSparse nzm m n cs rs xs mnz

-- | Read a CXSparse matrix, from a C-allocated pointer, then free the matrix.
unsafePeek :: Ptr CXSparse -> IO CXSparse
{-# INLINE unsafePeek #-}
unsafePeek p = do
  let pj = castPtr p :: Ptr Int
      pc = flip alignPtr sp $ p  `plusPtr` (3*si) :: Ptr (Ptr Int)
      pz = flip alignPtr si $ pc `plusPtr` (3*sp) :: Ptr Int
      si = sizeOf (undefined :: Int)
      sp = sizeOf (undefined :: Ptr ())
  nzm <- peek  pj
  m   <- peek (pj +>   si)
  n   <- peek (pj +> 2*si)
  mnz <- peekm pz
  cs  <- peekv (maybe (n+1) id mnz) pc
  rs  <- peekv nzm (pc +> sp)
  xs  <- peekv nzm (pc `plusPtr` (2*sp))
  cs_free p
  return $ CXSparse nzm m n cs rs xs mnz

------------------------------------------------------------------------------
-- | Use (-1) as a sentinel, when reading a `Maybe` value.
peekm :: (Storable a, Num a, Eq a) => Ptr a -> IO (Maybe a)
{-# INLINE peekm #-}
peekm p = peek p >>= \n -> return $ bool (Just n) Nothing (n == -1)


-- ** Read data from pointers to `Vector's.
------------------------------------------------------------------------------
peekv :: Storable a => Int -> Ptr (Ptr a) -> IO (Vector a)
{-# INLINE peekv #-}
peekv n p = peek p >>= vpeek n

vpeek :: Storable a => Int -> Ptr a -> IO (Vector a)
{-# INLINE vpeek #-}
-- vpeek n = fmap (flip Vec.unsafeFromForeignPtr0 n) . newForeignPtr_
-- vpeek n = fmap (flip Vec.unsafeFromForeignPtr0 n) . newForeignPtr finalizerFree
vpeek n = fmap (flip Vec.unsafeFromForeignPtr0 n) . newForeignPtr p_cs_free
