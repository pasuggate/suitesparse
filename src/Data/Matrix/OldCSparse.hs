{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE CPP, ForeignFunctionInterface #-}
{-# LANGUAGE Rank2Types, ScopedTypeVariables #-}
module Data.Matrix.CSparse where

------------------------------------------------------------------------------
--
--  Operations useful for writing monadic, sparse-matrix code.
--
--  Changelog:
--   + 04/09/2015  --  initial file;
--

import GHC.Types (SPEC(..))
-- import GHC.Int
import Foreign as F
import Foreign.C.Types
-- import qualified Foreign.Concurrent as Con
import System.IO.Unsafe
import Data.Bool
import Data.Vector.Storable (Vector)
import qualified Data.Vector.Storable as Vec
-- import qualified Data.Vector.Generic as G
import Data.Vector.Helpers as H


-- * Concise Sparse-matrix library data-structures.
------------------------------------------------------------------------------
data CSparse = CSparse
  { cs'nzmax :: Int
  , cs'rowN  :: Int
  , cs'colN  :: Int
  , cs'colp  :: Vector Int
  , cs'rowi  :: Vector Int
  , cs'xs    :: Vector Double
  , cs'nz    :: Maybe Int
  } deriving (Eq, Show)

data CSparseRaw = CSparseRaw
  { csr'nzmax :: CInt
  , csr'm     :: CInt
  , csr'n     :: CInt
  , csr'p     :: Ptr CInt
  , csr'i     :: Ptr CInt
  , csr'x     :: Ptr CDouble
  , csr'nz    :: CInt
  }


-- * Some instances for the data-structures.
------------------------------------------------------------------------------
instance Storable CSparse where
  {-# INLINE sizeOf #-}
  sizeOf  _ = 4*sizeOf (undefined :: Int) +
              3*sizeOf (undefined :: Ptr ())
  {-# INLINE alignment #-}
  alignment = const (sizeOf (undefined :: Ptr ()))
  -- NOTE: Copies the arrays on `peek`.
  peek p = do
    let pj = castPtr p :: Ptr Int
        pc = flip alignPtr sp $ p  `plusPtr` (3*si) :: Ptr (Ptr Int)
        pz = flip alignPtr si $ pc `plusPtr` (3*sp) :: Ptr Int
        si = sizeOf (undefined :: Int)
        sp = sizeOf (undefined :: Ptr ())
    nzm <- peek  pj
    m   <- peek (pj +>   si)
    n   <- peek (pj +> 2*si)
    mnz <- peekm pz
    cs  <- vcopy (maybe (n+1) id mnz) =<< peek pc
    rs  <- vcopy nzm =<< peek (pc +> sp)
    xs  <- vcopy nzm =<< peek (pc `plusPtr` (2*sp))
    return $ CSparse nzm m n cs rs xs mnz
  poke p (CSparse nzmax m n cs rs xs mnz) = do
    let pj = castPtr p :: Ptr Int
        si = sizeOf (undefined :: Int)
        sp = sizeOf (undefined :: Ptr ())
        pc = flip alignPtr sp $ p `plusPtr` (3*si) :: Ptr (Ptr Int)
        px = pc `plusPtr` (2*sp) :: Ptr (Ptr Double)
--         px = pc `plusPtr` (2*sp) :: Ptr (Ptr CDouble)
        nz = maybe (-1) fi mnz :: Int
        pn = castPtr $ pc +> 3*sp :: Ptr Int
    poke pj nzmax >> poke (pj +> si) m >> poke (pj +> 2*si) n
    cs `Vec.unsafeWith` poke pc
    rs `Vec.unsafeWith` poke (pc +> sp)
    xs `Vec.unsafeWith` poke px
    poke pn nz

{-- }
instance Storable CSparse where
  {-# INLINE sizeOf #-}
  sizeOf  _ = 48
--   sizeOf  _ = 4*sizeOf (undefined :: Int) +
--               3*sizeOf (undefined :: Ptr ())
  {-# INLINE alignment #-}
  alignment = const (sizeOf (undefined :: Ptr ()))
  peek p = peek (castPtr p) >>= csr2cs
{-- }
  peek p = do
    let pj = castPtr p :: Ptr CInt
        si = sizeOf (undefined :: CInt)
        sp = sizeOf (undefined :: Ptr ())
        pc = flip alignPtr sp $ p `plusPtr` (3*si) :: Ptr (Ptr CInt)
        pr = pc +> sp :: Ptr (Ptr CInt)
        px = pr `plusPtr` sp :: Ptr (Ptr CDouble)
        pz = px `plusPtr` sp :: Ptr CInt
    nzm <- fi `fmap` peek pj :: IO Int
    m   <- fi `fmap` peek (pj +> si)
    n   <- fi `fmap` peek (pj +> 2*si)
    rs  <- Vec.map fi `fmap` peekv nzm pr
    xs  <- Vec.map realToFrac `fmap` peekv nzm px
    mnz <- fmap fi `fmap` peekm pz
    cs  <- case mnz of
      Nothing -> Vec.map fi `fmap` peekv (n+1) pc
      Just  _ -> Vec.map fi `fmap` peekv  nzm  pc
    return $ CSparse nzm m n cs rs xs mnz
--}
  poke p (CSparse nzmax m n cs rs xs mnz) = do
    let pj = castPtr p :: Ptr CInt
        si = sizeOf (undefined :: CInt)
        sp = sizeOf (undefined :: Ptr ())
        pc = flip alignPtr sp $ p `plusPtr` (3*si) :: Ptr (Ptr CInt)
        pr = pc `plusPtr` sp :: Ptr (Ptr CInt)
        px = pr `plusPtr` sp :: Ptr (Ptr CDouble)
        nz = maybe (-1) fi mnz :: CInt
        pn = castPtr $ pc +> 3*sp :: Ptr CInt
    poke pj (fi nzmax) >> poke (pj +> si) (fi m) >> poke (pj +> 2*si) (fi n)
    Vec.map fi cs `Vec.unsafeWith` poke pc
    Vec.map fi rs `Vec.unsafeWith` poke pr
    Vec.map realToFrac xs `Vec.unsafeWith` poke px
    poke pn nz
--}

instance Storable CSparseRaw where
  {-# INLINE sizeOf #-}
  sizeOf  _ = 48
  {-# INLINE alignment #-}
  alignment = const (sizeOf (undefined :: Ptr ()))
  peek p = do
    let pj = castPtr p :: Ptr CInt
        si = sizeOf (undefined :: CInt)
        sp = sizeOf (undefined :: Ptr ())
        pc = flip alignPtr sp $ p `plusPtr` (3*si) :: Ptr (Ptr CInt)
        pr = pc +> sp :: Ptr (Ptr CInt)
        px = pr `plusPtr` sp :: Ptr (Ptr CDouble)
        pz = px `plusPtr` sp :: Ptr CInt
    nzm <- peek pj :: IO CInt
    m   <- peek (pj +> si)
    n   <- peek (pj +> 2*si)
    cp  <- peek pc
    rp  <- peek pr
    xp  <- peek px
    mnz <- peek pz
    return $ CSparseRaw nzm m n cp rp xp mnz
  poke p (CSparseRaw nzmax m n cp rp xp mnz) = do
    let pj = castPtr p :: Ptr CInt
        si = sizeOf (undefined :: CInt)
        sp = sizeOf (undefined :: Ptr ())
        pc = flip alignPtr sp $ p `plusPtr` (3*si) :: Ptr (Ptr CInt)
        pr = pc `plusPtr` sp :: Ptr (Ptr CInt)
        px = pr `plusPtr` sp :: Ptr (Ptr CDouble)
        pn = castPtr $ pc +> 3*sp :: Ptr CInt
    poke pj nzmax >> poke (pj +> si) m >> poke (pj +> 2*si) n
    poke pc cp
    poke pr rp
    poke px xp
    poke pn mnz

csr2cs :: CSparseRaw -> IO CSparse
csr2cs (CSparseRaw nzmax m n cp rp xp mnz) = do
  let nzm  = fi nzmax
      m'   = fi m
      n'   = fi n
      mnz' = bool (Just $ fi mnz) Nothing (mnz == (-1))
      cnz  = case mnz' of
        Nothing -> n'+1
        Just  _ -> nzm
  cs <- Vec.map fi <$> vpeek cnz cp
  rs <- Vec.map fi <$> vpeek nzm rp
  xs <- Vec.map realToFrac <$> vpeek nzm xp
  return $ CSparse nzm m' n' cs rs xs mnz'


-- ** Read data from pointers to `Vector's.
------------------------------------------------------------------------------
peekv :: Storable a => Int -> Ptr (Ptr a) -> IO (Vector a)
{-# INLINE peekv #-}
peekv n p = peek p >>= vpeek n

vpeek :: Storable a => Int -> Ptr a -> IO (Vector a)
{-# INLINE vpeek #-}
-- vpeek n = fmap (flip Vec.unsafeFromForeignPtr0 n) . newForeignPtr_
-- vpeek n = fmap (flip Vec.unsafeFromForeignPtr0 n) . newForeignPtr finalizerFree
vpeek n = fmap (flip Vec.unsafeFromForeignPtr0 n) . newForeignPtr p_cs_free

vcopy :: forall a. Storable a => Int -> Ptr a -> IO (Vector a)
{-# INLINE vcopy #-}
vcopy n p = do
  ar <- H.new n
  let go !_ !q !r | r  <  n   = peek q >>= wr ar r >> go SPEC (q+>si) (r+1)
                  | otherwise = frz ar
      si = sizeOf (undefined :: a)
  go SPEC p 0


-- * Foreign bindings.
------------------------------------------------------------------------------
foreign import ccall "suitesparse/cs.h cs_spfree"
  cs_spfree :: Ptr CSparse -> IO ()

foreign import ccall "suitesparse/cs.h cs_free"
  cs_free :: Ptr a -> IO ()

foreign import ccall "suitesparse/cs.h &cs_free"
  p_cs_free :: FunPtr (Ptr a -> IO ())

foreign import ccall "suitesparse/cs.h &cs_spfree"
  p_cs_spfree :: FunPtr (Ptr CSparse -> IO ())

foreign import ccall "suitesparse/cs.h cs_add"
  cs_add :: Ptr CSparse -> Ptr CSparse -> CDouble -> CDouble -> Ptr CSparse

foreign import ccall "suitesparse/cs.h cs_multiply"
  cs_multiply :: Ptr CSparse -> Ptr CSparse -> Ptr CSparse

foreign import ccall "suitesparse/cs.h cs_transpose"
  cs_transpose :: Ptr CSparse -> CInt -> Ptr CSparse


cs'spfree :: Ptr CSparse -> IO ()
cs'spfree p = return $ const () (cs_spfree p)

cs'copy :: CSparse -> CSparse
cs'copy cs = unsafePerformIO $ F.new cs >>= peek

cs'add :: CSparse -> CSparse -> Double -> Double -> CSparse
cs'add a b alpha beta = unsafePerformIO $ do
  pa <- F.new a
  pb <- F.new b
  cspeek $ cs_add pa pb (realToFrac alpha) (realToFrac beta)

cs'multiply :: CSparse -> CSparse -> CSparse
cs'multiply a b = unsafePerformIO $ cs_multiply <$> F.new a <*> F.new b >>= cspeek

cs'transpose :: CSparse -> Bool -> CSparse
cs'transpose m vals = unsafePerformIO $ do
  p <- F.new m
  cspeek $ cs_transpose p (bool 0 1 vals)


foreign import ccall "suitesparse/cs.h cs_add"
  csr_add :: Ptr CSparseRaw -> Ptr CSparseRaw -> CDouble -> CDouble -> Ptr CSparseRaw


csr'add :: CSparseRaw -> CSparseRaw -> CDouble -> CDouble -> CSparseRaw
csr'add a b alpha beta = unsafePerformIO $ do
  ap <- F.new a
  bp <- F.new b
  peek $ csr_add ap bp alpha beta


-- * Helper functions.
------------------------------------------------------------------------------
fpeek :: Storable a => Ptr a -> IO a
{-# INLINE fpeek #-}
fpeek p = newForeignPtr finalizerFree p >>= flip withForeignPtr peek

-- cspeek :: Ptr CSparse -> IO CSparse
-- {-# INLINE cspeek #-}
-- cspeek p = peek p >>= \m -> cs_spfree p >> return m

cspeek :: Ptr CSparse -> IO CSparse
{-# INLINE cspeek #-}
cspeek p = do
  let pj = castPtr p :: Ptr Int
      pc = flip alignPtr sp $ p  `plusPtr` (3*si) :: Ptr (Ptr Int)
      pz = flip alignPtr si $ pc `plusPtr` (3*sp) :: Ptr Int
      si = sizeOf (undefined :: Int)
      sp = sizeOf (undefined :: Ptr ())
  nzm <- peek  pj
  m   <- peek (pj +>   si)
  n   <- peek (pj +> 2*si)
  mnz <- peekm pz
  cs  <- peekv (maybe (n+1) id mnz) pc
  rs  <- peekv nzm (pc +> sp)
  xs  <- peekv nzm (pc `plusPtr` (2*sp))
  cs_free p
  return $ CSparse nzm m n cs rs xs mnz

fi :: (Integral i, Num a) => i -> a
{-# INLINE fi #-}
fi  = fromIntegral

------------------------------------------------------------------------------
-- | Pointer arithmetic, and without recasting.
infixl 6 +>

(+>) :: Ptr a -> Int -> Ptr a
{-# INLINE (+>) #-}
(+>)  = plusPtr

------------------------------------------------------------------------------
-- | Use (-1) as a sentinel, when reading a `Maybe` value.
peekm :: (Storable a, Num a, Eq a) => Ptr a -> IO (Maybe a)
{-# INLINE peekm #-}
peekm p = peek p >>= \n -> return $ bool (Just n) Nothing (n == -1)

{-- }
ccs2csrD :: (Real a, Storable a) => CCSMat a -> CSparseRaw
{-# INLINE ccs2csrD #-}
ccs2csrD (CCS rn ri cp xs) = unsafePerformIO $ do
  let nzm = fi $ len ri
      m   = fi rn
      n   = fi $ len cp - 1
  Vec.unsafeWith (G.map fi cp) $ \p -> do
    Vec.unsafeWith (G.map fi ri) $ \i -> do
      Vec.unsafeWith (G.map realToFrac xs) $ \x -> do
        return $ CSparseRaw nzm m n p i x (-1)

csr2ccsD :: (Fractional a, Storable a) => CSparseRaw -> CCSMat a
{-# INLINE csr2ccsD #-}
csr2ccsD (CSparseRaw nzm m n p i x nz) = unsafePerformIO $ do
  let rn  = fi m
      cn  = fi n
  cp <- G.map fi <$> vpeek (cn+1) p
  ri <- G.map fi <$> vpeek (fi nzm) i
  xs <- G.map realToFrac <$> vpeek (fi nzm) x
  return $ CCS rn ri cp xs
--}
